#!/bin/sh

if [ -z "$1" ]
  then
    echo "No trace supplied, add it as an argument of this script"
    exit 1
fi

../../../../build/harm --clk clk --vcd "$1" -c rsa100.xml --vcd_ss ::testbench::uut
