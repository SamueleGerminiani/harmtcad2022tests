#!/bin/bash

for input in "$@"
do
    ass="$(grep -o '(.*)$' $input)"
    allass+=$ass
    allass+=$'\n'
    te="$(grep -m 1 -o '>.(.*)' $input)"
    tvarsBool="$(echo $te | grep  -o '[a-zA-Z_]\+[0-9]*\?\s\+')"
    tvarsLogic="$(echo $te | grep -o '[a-zA-Z_]\+[0-9]*\?\[[0-9]\+\]')"
    targetVar+=$tvarsBool
    targetVar+=" "
    targetVar+=$tvarsLogic
    targetVar+=" "
    comTV+=$targetVar
    comTV+=' '
    targetVar=""


    varsBool+="$(echo $ass | grep -o '[a-zA-Z_]\+[0-9]*\?\s\+')"
    varsBool+=" "
    varsLogic+="$(echo $ass | grep -o '[a-zA-Z_]\+[0-9]*\?\[[0-9]*\]')"
    varsLogic+=" "

done


comTV="$(echo $comTV | tr ' ' '\n' | sort)"

uvarsBool="$(echo $varsBool | tr ' ' '\n' | sort | uniq)"
uvarsLogic="$(echo $varsLogic | tr ' ' '\n' | sort | uniq)"
uvars+=$uvarsBool
uvars+=' '
uvars+=$uvarsLogic
varsList="$(echo $uvars | tr ' ' '\n')"

echo "" > harm.xml
echo "<harm>" >> harm.xml
echo "  <context name=\"default\">" >> harm.xml
echo "" >> harm.xml

while IFS= read -r line
do
        if [[ $comTV != *"$line"* ]]; then
            echo "      <prop exp=\"$line\" loc=\"a\"/>" >> harm.xml
        fi
done < <(printf '%s\n' "$varsList")

echo "" >> harm.xml

while IFS= read -r line
do
            echo "      <prop exp=\"$line\" loc=\"c\"/>" >> harm.xml
done < <(printf '%s\n' "$comTV")

echo "" >> harm.xml
echo "      <template bdtLimits=\"[(3,5,5),-0.1,R,1]\" exp=\"G({..#1&..}|-> P0)\" />" >> harm.xml

echo "  </context>" >> harm.xml

echo "</harm>" >> harm.xml



echo "" > gm.xml
echo "<harm>" >> gm.xml
echo "  <context name=\"default\">" >> gm.xml
echo "" >> gm.xml

while IFS= read -r line
do
    if [[ $line != "a" ]]; then
        if [[ $line != $targetVar  ]]; then
            sub=$(echo "$line" | sed "s/|->/}|->{/")
            sub=$(echo "$sub" | sed "s/|=>/}|=>{/")
            sub=$(echo "$sub" | sed "s/&/&&/g")
            echo "      <template exp=\"G({$sub})\" />" >> gm.xml
        fi
    fi
done < <(printf '%s\n' "$allass")

echo "  </context>" >> gm.xml

echo "</harm>" >> gm.xml
cat harm.xml
