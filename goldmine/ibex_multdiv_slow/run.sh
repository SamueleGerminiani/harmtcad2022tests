#!/bin/sh

if [ -z "$1" ]
  then
    echo "No trace supplied, add it as an argument of this script"
    exit 1
fi

../../../../build/harm --clk clk_i --vcd "$1" -c harm.xml
../../../../build/harm --clk clk_i --vcd "$1" -c gm.xml  
